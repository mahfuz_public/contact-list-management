/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mahfuz.contactmanagement;

import com.mahfuz.contactmanagement.controllers.InterfaceMainController;
import com.mahfuz.contactmanagement.controllers.MainController;
import com.mahfuz.contactmanagement.dal.DBLayerFactory;
import com.mahfuz.contactmanagement.network.Server;
import com.mahfuz.contactmanagement.uis.ContactUIController;
import com.mahfuz.contactmanagement.uis.LoginUIController;
import com.mahfuz.contactmanagement.utilities.PropertyReader;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author itbdlive
 */
public class ApplicationLauncher extends Application{
    
    MainController mainController = null;
    
    public static Stage stage;
    
    @Override
    public void start(Stage stage) throws Exception {
        
//        Server server = new Server();
//        server.handleServer();
        
        ApplicationLauncher.stage = stage;
        
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/mahfuz/contactmanagement/uis/LoginUI.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        String logo = PropertyReader.getProperty("config.properties", "logo");
        Image img = new Image(logo);
        stage.getIcons().add(img);
        String com_name = PropertyReader.getProperty("config.properties", "com_name");
        stage.setTitle(com_name);
        
        stage.setScene(scene);
        
       LoginUIController loginuiController = loader.<LoginUIController>getController();
       mainController = new MainController();
       loginuiController.setController(mainController);
       mainController.setLoginUI(loginuiController);
       mainController.setDBLayer(DBLayerFactory.getDBLayer());
       System.out.println("application launcher");
        
       
       stage.show();
    }
    
//    public void beginApp(){
//        this.mainController = new MainController();
//        System.out.println("In main - calling beginapp");
//        launch();
//    }
    
}
