/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mahfuz.contactmanagement.connectivity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author itbdlive
 */
public class ConnectionClass {
    public Connection connection;
    public Connection getConnection() throws Exception{
        String dbName = "cms";
        String dbUser = "root";
        String dbpass = "";
        
        try{
          Class.forName("com.mysql.jdbc.Driver");  
          connection = DriverManager.getConnection("jdbc:mysql://localhost/"+dbName,dbUser,dbpass);
          
        }
        catch(ClassNotFoundException | SQLException e){
            System.out.println(e.getMessage());
        }
            

        return connection;
    }
    
}
