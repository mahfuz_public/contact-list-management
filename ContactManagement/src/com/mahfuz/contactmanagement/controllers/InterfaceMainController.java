/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mahfuz.contactmanagement.controllers;

import com.mahfuz.contactmanagement.models.Contacts;
import com.mahfuz.contactmanagement.uis.ContactUIController;
import com.mahfuz.contactmanagement.uis.InterfaceContactUI;
import java.io.File;
import java.util.ArrayList;
import javafx.collections.ObservableList;

/**
 *
 * @author itbdlive
 */
public interface InterfaceMainController {
    
    void checkLogin(String userid, String password);
    void addContact(String name, String phone, String email, String address, String city, String country);

    public void setContactUI(InterfaceContactUI icontactui);

    public ArrayList<Contacts> getContacts();
    

    public void batchUpload(File fileName);

    
    
}
