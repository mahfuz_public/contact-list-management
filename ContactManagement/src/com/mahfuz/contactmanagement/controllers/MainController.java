/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mahfuz.contactmanagement.controllers;

import com.mahfuz.contactmanagement.dal.AbstractDBLayer;
import com.mahfuz.contactmanagement.dal.MySQLdblayer;
import com.mahfuz.contactmanagement.models.Contacts;
import com.mahfuz.contactmanagement.models.Users;
import com.mahfuz.contactmanagement.uis.InterfaceContactUI;
import com.mahfuz.contactmanagement.uis.InterfaceLoginUI;
import com.mahfuz.contactmanagement.uis.LoginUIController;
import com.mahfuz.contactmanagement.utilities.reader.ExcelFileReader;
import com.mahfuz.contactmanagement.utilities.reader.TextFileReader;
import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import javafx.collections.ObservableList;
import org.apache.log4j.Logger;
//import java.util.logging.Level;
//import java.util.logging.Logger;


/**
 *this is the main controller that will be loaded by the
 * main class - this class will load the login UI if user is not logged in 
 * @author itbdlive
 */
public class MainController implements InterfaceMainController{
    
    InterfaceLoginUI LoginUI = null;
    InterfaceContactUI ContactUI = null;
    private static final Logger LOGGER = Logger.getLogger(MainController.class);
    
    AbstractDBLayer dbl= null;
    ArrayList<Contacts>list;
    
    @Override
    public void checkLogin(String userid, String password){
        LOGGER.debug("In userexits method: "+userid);
        Users u = new Users();
        u.setUserid(userid);
        u.setPassword(password);
        System.out.println("check login print");
        
        try 
        {
          if(dbl.userExists(u))
          {
              LOGGER.info("user exists");
              LoginUI.loginSuccess();
          }
          else
          {
              LoginUI.loginFailed();
          }
        } catch (Exception ex) {
            //Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
            LOGGER.error("error is "+ex);
        }
        
        

    }
    
    @Override
    public void addContact(String name, String phone, String email, String address, String city, String country){
        Contacts contact = new Contacts();
        contact.setName(name);
        contact.setPhone(phone);
        contact.setEmail(email);
        contact.setAddress(address);
        contact.setCity(city);
        contact.setCountry(country);
       
        
       // dbl =new MySQLdblayer();
       
        try {
            boolean ck = dbl.checkInsertContact(contact);
            if(ck)
        {
            ContactUI.addSuccess();
        }
        else
        {
            ContactUI.addFailed();
        }
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public void setLoginUI(InterfaceLoginUI loginui) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        this.LoginUI = loginui;
    }
    
    public void setContactUI(InterfaceContactUI icontactui)
    {
        this.ContactUI = icontactui;
    }
    public void setDBLayer(AbstractDBLayer dblayer)
    {
        this.dbl = dblayer;
        if(dblayer == null)
        {
            System.out.println("db layer is null");
        }
        System.out.println("jguyiuyyuy");
    }
    
    @Override
    public ArrayList<Contacts>getContacts() {
        
         try {
             System.out.println("calling getcntacts in maincontroller");
             //dbl = MySQLdblayer.getDBLayer();
             ArrayList<Contacts>contacts=dbl.getAllContacts();
             
             if(contacts!=null){
             return contacts;
             }
         } catch (Exception ex) {
             ContactUI.errorMessageSent(ex.getMessage());
            System.out.println();
         }
         return null;
    }

    

    
    
    @Override
    public void batchUpload(File fileName)
    {
        String filename = fileName.getName().toLowerCase();
        if(filename.endsWith(".txt"))
        {
           TextFileReader txtFile = new TextFileReader();
           list = txtFile.txtProcessfile(fileName);
           for(Contacts contact:list)
           {
           System.out.println(contact.getName()+" , "+contact.getPhone());
           }
           
           
        }
        else if(filename.endsWith(".xls"))
        {
           ExcelFileReader xlsFile = new ExcelFileReader();
           xlsFile.xlsProcessfile(fileName);
        }
    }
    

    
}
