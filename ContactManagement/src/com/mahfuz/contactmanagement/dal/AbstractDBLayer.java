/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mahfuz.contactmanagement.dal;

import com.mahfuz.contactmanagement.models.Contacts;
import com.mahfuz.contactmanagement.models.Users;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javafx.collections.ObservableList;

/**
 *
 * @author itbdlive
 */
public abstract class AbstractDBLayer {
    
    
    protected Statement st;
    protected String dbhost;
    protected String dbname;
    protected String dbuser;
    protected String dbpass;
    protected String fullconnString;
    protected Connection con = null;
    
    public abstract AbstractDBLayer getDBLayer();
    
    public abstract boolean userExists(Users u) throws Exception;
    public abstract boolean checkInsertContact(Contacts contact) throws Exception;
    public abstract ArrayList<Contacts> getAllContacts()throws Exception;
    public abstract void dbconnect();
   
    
    protected void closeConn() throws SQLException
    {
        try
        {
           if(con != null)
            {
            con.close();
            } 
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
        
    }

}
