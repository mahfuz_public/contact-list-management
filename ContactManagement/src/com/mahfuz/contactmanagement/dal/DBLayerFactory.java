/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mahfuz.contactmanagement.dal;

import com.mahfuz.contactmanagement.utilities.PropertyReader;

/**
 *
 * @author itbdlive
 */
public class DBLayerFactory {
    static AbstractDBLayer db=null;
    public static AbstractDBLayer getDBLayer() throws Exception
    {
       String dbName = PropertyReader.getProperty("config.properties", "dbName");
       Class c=Class.forName(dbName);
       Object obj=c.newInstance();
       db=(AbstractDBLayer) obj;
       return db;
    }
}
