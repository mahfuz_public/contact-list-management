/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mahfuz.contactmanagement.dal;

import com.mahfuz.contactmanagement.connectivity.ConnectionClass;
import com.mahfuz.contactmanagement.models.Contacts;
import com.mahfuz.contactmanagement.models.Users;
import com.mahfuz.contactmanagement.utilities.PropertyReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javafx.collections.ObservableList;

/**
 *
 * @author itbdlive
 */
public class PGdblayer extends AbstractDBLayer
{
    
    
    @Override
    public boolean userExists(Users u) throws SQLException, Exception {
        
//        ConnectionClass connectionClass = new ConnectionClass();
//        Connection conn = connectionClass.getConnection();
//        Statement st = conn.createStatement();
        dbconnect();
        
        try
        {
            ResultSet result=st.executeQuery("SELECT * FROM users where user_id='"+u.getUserid()+"'and password='"+u.getPassword()+"';");
            
            if(result.next())
            {
                if(u.getUserid().equals(result.getString(2)) && u.getPassword().equals(result.getString(3)) )
                {
                    u.setUsertype(result.getString(4));
                    return true;
                }
            }   
        }
        catch(SQLException e)
        {
            e.getMessage();
        }
        
        closeConn();

        return false;
    }
    
    /**
     *
     * @param contact
     * @return
     * @throws SQLException
     */
    @Override
    public boolean checkInsertContact(Contacts contact) throws Exception
    {
        System.out.println("in DBLayer contact add in db.");
        dbconnect();
        String query = "insert into contacts values("+contact.getId()+",'"+contact.getName()+"','"+contact.getPhone()+"','"+contact.getEmail()+"','"+contact.getAddress()+"','"+contact.getCity()+"','"+contact.getCountry()+"');";
        int result = st.executeUpdate(query);
        if(result>0)
        {
            return true;
        }
        closeConn();
       
        return false;
    }
    
    @Override
    public ArrayList<Contacts> getAllContacts()throws Exception
    {
        System.out.println("calling after maincontroller");
         dbconnect();
         ArrayList<Contacts>contacts=new ArrayList<>();
         String query="select * from contacts;";
         ResultSet rs=st.executeQuery(query);
         while(rs.next())
         {
             int id=rs.getInt("id");
             String name=rs.getString("name");
             String phone=rs.getString("phone");
             String email=rs.getString("email");
             String address=rs.getString("address");
             String city=rs.getString("city");
             String country=rs.getString("country");
             
             Contacts contact=new Contacts(id,name,phone,email,address,city,country);
             contacts.add(contact);  
         }   
        return contacts;
    }

    
    @Override
    public void dbconnect()
    {
        try
        {
           // Class.forName("com.mysql.jdbc.Driver");
            dbhost = PropertyReader.getProperty("config.properties", "dbhost");
            dbname = PropertyReader.getProperty("config.properties", "dbname");
            dbuser = PropertyReader.getProperty("config.properties", "dbuser");
            dbpass = PropertyReader.getProperty("config.properties", "dbpass");
            fullconnString = "jdbc:postgresql://"+dbhost+"/"+dbname;
            System.out.println("DBHOSTINFO:"+ fullconnString);
            con=DriverManager.getConnection(fullconnString,dbuser,dbpass);
            st = con.createStatement();
        }
        catch(Exception e)
        {
            e.getMessage();
        }
    }

    @Override
    public AbstractDBLayer getDBLayer() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return this;
    }
    
    
        

}
