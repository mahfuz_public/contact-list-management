/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mahfuz.contactmanagement.models;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author itbdlive
 */
public class Item {
    private final IntegerProperty id;
    private final StringProperty name;
    private final StringProperty phone;
    private final StringProperty email;
    private final StringProperty address;
    private final StringProperty city;
    private final StringProperty country;
        

    public Item(int id, String name, String phone, String email,String address, String city, String country) {
       this.id = new SimpleIntegerProperty(id);
       this.name = new SimpleStringProperty(name);
       this.phone = new SimpleStringProperty(phone);
       this.email = new SimpleStringProperty(email);
       this.address = new SimpleStringProperty(address);
       this.city = new SimpleStringProperty(city);
       this.country = new SimpleStringProperty(country);
       
    }

    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }
    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getPhone() {
        return phone.get();
    }

    public void setPhone(String phone) {
        this.phone.set(phone);
    }
    public String getEmail() {
        return email.get();
    }

    public void setEmail(String email) {
        this.email.set(email);
    }
    public String getAddress() {
        return address.get();
    }

    public void setAddress(String address) {
        this.address.set(address);
    }
    public String getCity() {
        return city.get();
    }

    public void setCity(String city) {
        this.city.set(city);
    }
    public String getCountry() {
        return country.get();
    }

    public void setCountry(String country) {
        this.country.set(country);
    }
    
    
    public IntegerProperty idProperty()
    {
        return id;
    }
    public StringProperty nameProperty()
    {
        return name;
    }
    public StringProperty phoneProperty()
    {
        return phone;
    } 
    public StringProperty emailProperty()
    {
        return email;
    } 
    public StringProperty addressProperty()
    {
        return address;
    } 
    public StringProperty cityProperty()
    {
        return city;
    } 
    public StringProperty countryProperty()
    {
        return country;
    } 
}
