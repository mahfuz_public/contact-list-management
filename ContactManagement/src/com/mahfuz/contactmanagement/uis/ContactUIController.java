package com.mahfuz.contactmanagement.uis;

import static com.mahfuz.contactmanagement.ApplicationLauncher.stage;
import com.mahfuz.contactmanagement.controllers.InterfaceMainController;
import com.mahfuz.contactmanagement.models.Contacts;
import com.mahfuz.contactmanagement.models.Item;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author itbdlive
 */
public class ContactUIController implements Initializable , InterfaceContactUI{
    
    InterfaceMainController mainController = null;
    private final TableView<Contacts> tblViewer = new TableView();
    private ArrayList<Contacts>contacts=new ArrayList<>();
    ObservableList<Item> list=FXCollections.observableArrayList();
    
    @FXML private TextField txname;
    @FXML private TextField txphone;
    @FXML private TextField txemail;
    @FXML private TextField txaddress;
    @FXML private TextField txcity;
    @FXML private TextField txcountry;
    @FXML private TableView tableview;
    @FXML private TableView<Item> tbl_contact;
    @FXML private TableColumn<Contacts, String> tbl_id;
    @FXML private TableColumn<Item, String> tbl_name;
    @FXML private TableColumn<Item, String> tbl_phone;
    @FXML private TableColumn<Contacts, String> tbl_email;
    @FXML private TableColumn<Contacts, String> tbl_address;
    @FXML private TableColumn<Contacts, String> tbl_city;
    @FXML private TableColumn<Contacts, String> tbl_country;
    
    
    
    @FXML private TextField txsearch;
    @FXML private TextField labelfilereader;
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    
    @FXML
    private void saveContactAction(ActionEvent event) 
    {
        String name = txname.getText();
        String phone = txphone.getText();
        String email = txemail.getText();
        String address = txaddress.getText();
        String city = txcity.getText();
        String country = txcountry.getText();
        
        mainController.addContact(name, phone, email, address, city, country);
        System.out.println("button action start");
    }
    
    @FXML
    private void fileUpload(ActionEvent event) 
    {
        FileChooser fileChooser = new FileChooser();
        File fileName = fileChooser.showOpenDialog(stage);
        if(fileName != null)
        { 
            System.out.println("selected file ::" + fileName);
        }
        mainController.batchUpload(fileName);
        
       
        
        
    }
    
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        
    } 

    @Override
    public void addSuccess() {
        System.out.println("contact added successfully");
    }

    @Override
    public void addFailed() {
        System.out.println("contact added failed");
    }

    @Override
    public void errorMessageSent(String msg) {
        System.out.println(msg);
    }

    void setController(InterfaceMainController mc) {
        this.mainController = mc;
        System.out.println("list show method calling.");
        System.out.println("after calling get contacts");
        
        loadListView();
        
           
    }
    
    protected void loadListView()
    {
        contacts= mainController.getContacts();
        for(int i=0; i<contacts.size();i++)
        {
              int id=contacts.get(i).getId();
              String name=contacts.get(i).getName();
              String phone=contacts.get(i).getPhone();
              String email=contacts.get(i).getEmail();
              String address=contacts.get(i).getAddress();
              String city=contacts.get(i).getCity();
              String country=contacts.get(i).getCountry();
              tbl_id.setCellValueFactory(
                new PropertyValueFactory<>("id")); 
              tbl_name.setCellValueFactory(
                new PropertyValueFactory<>("name")); 
              tbl_phone.setCellValueFactory(
                new PropertyValueFactory<>("phone")); 
              tbl_email.setCellValueFactory(
                new PropertyValueFactory<>("email")); 
              tbl_address.setCellValueFactory(
                new PropertyValueFactory<>("address")); 
              tbl_city.setCellValueFactory(
                new PropertyValueFactory<>("city")); 
              tbl_country.setCellValueFactory(
                new PropertyValueFactory<>("country")); 
              System.out.println(id+" "+name+" "+phone);

              list.add(new Item(id,name,phone,email,address,city,country));

              tbl_contact.setItems(list);
        }  
    }    
}

