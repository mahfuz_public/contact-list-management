/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mahfuz.contactmanagement.uis;

import com.mahfuz.contactmanagement.ApplicationLauncher;
import com.mahfuz.contactmanagement.controllers.InterfaceMainController;
import com.mahfuz.contactmanagement.controllers.MainController;
import com.mahfuz.contactmanagement.utilities.PropertyReader;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author itbdlive
 */
public class LoginUIController implements Initializable, InterfaceLoginUI {
    
    InterfaceMainController mainController = null;
    
    @FXML private TextField txt_username;
    
    @FXML private PasswordField txt_password;
    
    @FXML private Label loglabel;
    
    
    @FXML private void handleButtonAction(ActionEvent event) throws IOException 
    {
       String userid = txt_username.getText();
       String password = txt_password.getText();
       
       mainController.checkLogin(userid, password);   
    }
    
    public void loadContactUI() throws IOException
    {
        System.out.println("login ui is loaded");
        try
        {
            FXMLLoader loader= new FXMLLoader(getClass().getResource("/com/mahfuz/contactmanagement/uis/ContactUI.fxml"));
            Parent root1 = loader.load();
            Scene scene = new Scene(root1);
            Stage st = new Stage();
            String logo = PropertyReader.getProperty("config.properties", "logo");
            Image img = new Image(logo);
            st.getIcons().add(img);
            String com_name = PropertyReader.getProperty("config.properties", "com_name");
            st.setTitle(com_name);
            st.setScene(scene);  
            ContactUIController contactuiController = loader.<ContactUIController>getController();
            //this.mainController =new MainController();
            contactuiController.setController(mainController);
             mainController.setContactUI(contactuiController);
            ApplicationLauncher.stage.close();
            st.show();

        }
        catch(Exception e)
        {
            System.out.println("Error: "+ e.getMessage());
        }
        
    }

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }  
    
    public void setController(InterfaceMainController mc)
    {
        this.mainController = mc;
    }

    @Override
    public void showErrorMessage(String msg) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loginSuccess() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        System.out.println("Login Success");
        try {
            this.loadContactUI();
        } catch (IOException ex) {
            Logger.getLogger(LoginUIController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void loginFailed() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        System.out.println("Login Failed");
        loglabel.setText("wrong username or password");

    }
    
}
