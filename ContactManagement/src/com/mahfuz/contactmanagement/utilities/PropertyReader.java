/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mahfuz.contactmanagement.utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author itbdlive
 */
public class PropertyReader {
    
    public static String getProperty(String propfile, String key) throws IOException
	{
            Properties prop = new Properties();
            InputStream input = null;

        try
        {
            input = new FileInputStream(propfile);
                // load a properties file
            prop.load(input);
                // get the property value and print it out
            String opm= prop.getProperty(key);
            return opm;    
        }
        catch (IOException ex)
        {
            throw ex;
        }
        finally
        {
            if (input != null) 
            {
                try 
                {
                    input.close();
                } 
                catch (IOException e) 
                {
                    e.printStackTrace();
                }
            }
        }


	}
    
}
