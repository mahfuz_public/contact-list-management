/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mahfuz.contactmanagement.utilities.reader;

import com.mahfuz.contactmanagement.models.Contacts;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author itbdlive
 */
public class TextFileReader {
    
    BufferedReader bufferedReader;
    FileReader reader;
    String line = null;
    
    ArrayList<Contacts>contactlist = new ArrayList<Contacts>();
    
    
    public ArrayList<Contacts>txtProcessfile(File fileName)
    {
        try {
            reader = new FileReader(fileName);
            bufferedReader = new BufferedReader(reader);
            
            while((line = bufferedReader.readLine()) != null)
            {
                String lineArr[] = line.split("\\%\\%");
                Contacts contacts = new Contacts(lineArr[0],lineArr[1],lineArr[2],lineArr[3],lineArr[4],lineArr[5]);
                contactlist.add(contacts);
                
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TextFileReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TextFileReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return contactlist;
    }
}
