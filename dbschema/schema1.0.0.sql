
CREATE TABLE IF NOT EXISTS users
(
id int unsigned PRIMARY KEY AUTO_INCRAMENT,
user_id varchar(20),
password varchar(20),
usertype varchar(3)
); 
CREATE TABLE IF NOT EXISTS contacts
(
id int unsigned PRIMARY KEY AUTO_INCRAMENT,
last_name varchar(255),
first_name varchar(255),
phone_number varchar(255),
address varchar(255),
email varchar(255),
photo varchar(255)
); 
insert into users (user_id,password,usertype)values('admin','admin123','OWN');